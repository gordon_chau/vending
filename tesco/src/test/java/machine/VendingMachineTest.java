package machine;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class VendingMachineTest {
	@Test
	public void defaultStateIsOff() {
		VendingMachine test = new VendingMachine();
		assertFalse(test.isOn());
	}
	
	@Test
	public void turnsOn() {
		VendingMachine test = new VendingMachine();
		test.setOn();
		assertTrue(test.isOn());		
	}
	
	@Test
	public void validateTenPenceCoin() {
		VendingMachine test = new VendingMachine();
		assertTrue(test.validateMoney(0.10));		
	}
	
	@Test
	public void validateTwentyPenceCoin() {
		VendingMachine test = new VendingMachine();
		assertTrue(test.validateMoney(0.20));		
	}
	
	@Test
	public void validateFiftyPenceCoin() {
		VendingMachine test = new VendingMachine();
		assertTrue(test.validateMoney(0.50));		
	}
	
	@Test
	public void validatePoundCoin() {
		VendingMachine test = new VendingMachine();
		assertTrue(test.validateMoney(1.00));		
	}
	
	@Test
	public void invalidCoin() {
		VendingMachine test = new VendingMachine();
		assertFalse(test.validateMoney(0.01));		
	}
	
	@Test(expected=Exception.class)
	public void insertMoneyWithInvalidAmount() {
		VendingMachine test = new VendingMachine();
		test.insertMoney(0.01);
	}
	
	@Test
	public void validateBalance() {
		VendingMachine test = new VendingMachine();
		test.insertMoney(0.10);
		test.insertMoney(0.20);
		test.insertMoney(0.50);
		assertTrue(0.80 == test.getBalance());
	}
	
	@Test
	public void returnCoins() {
		VendingMachine test = new VendingMachine();
		Map<String, Integer> loadChange = new HashMap<String, Integer>();
		loadChange.put("TEN_PENCE", 5);
		loadChange.put("TWENTY_PENCE", 5);
		loadChange.put("FIFTY_PENCE", 5);
		loadChange.put("POUND", 5);		
		
		test.loadChange(loadChange);
		
		test.insertMoney(0.10);
		test.insertMoney(0.10);
		test.insertMoney(0.10);		
		test.insertMoney(0.20);
		test.insertMoney(0.50);
		
		Map<String, Integer> returnCoins = test.returnCoins();
		assertTrue(returnCoins.get("POUND") == 1);
		
		Map<String, Integer> change = test.getChange();
		assertTrue(change.get("TEN_PENCE") == 8);
		assertTrue(change.get("TWENTY_PENCE") == 6);
		assertTrue(change.get("FIFTY_PENCE") == 6);
		assertTrue(change.get("POUND") == 4);		

	}
	
	@Test
	public void changeForEightyPence() {
		VendingMachine test = new VendingMachine();
		Map<String, Integer> change = test.getChange(0.80);
		assertTrue(change.get("TEN_PENCE") == 1);
		assertTrue(change.get("TWENTY_PENCE") == 1);
		assertTrue(change.get("FIFTY_PENCE") == 1);
	}
	
	@Test
	public void getItemA() {
		List<Item> items  = Arrays.asList(new Item("A", 10, 0.60));
		VendingMachine test = new VendingMachine(items);
		test.insertMoney(0.50);
		test.insertMoney(0.20);
		Item item = test.getItem("A");
		assertTrue(item.getQuantity() == 9);
		assertTrue(test.returnCoins().get("TEN_PENCE") == 1);
		assertTrue(test.getChange().get("TEN_PENCE") == 0);
	}
	
	@Test
	public void getItemB() {
		List<Item> items  = Arrays.asList(new Item("B", 10, 1.00));
		VendingMachine test = new VendingMachine(items);
		test.insertMoney(1.00);
		test.insertMoney(0.20);
		Item item = test.getItem("B");
		assertTrue(item.getQuantity() == 9);
		assertTrue(test.returnCoins().get("TWENTY_PENCE") == 1);
		assertTrue(test.getChange().get("TWENTY_PENCE") == 0);
	}
	
	@Test
	public void getItemC() {
		List<Item> items  = Arrays.asList(new Item("C", 10, 1.70));
		VendingMachine test = new VendingMachine(items);
		test.insertMoney(1.00);
		test.insertMoney(1.00);
		test.insertMoney(1.00);
		Item item = test.getItem("C");
		assertTrue(item.getQuantity() == 9);
		assertTrue(test.returnCoins().get("TEN_PENCE") == 1);
		assertTrue(test.getChange().get("TEN_PENCE") == 0);
	}
}
