package machine;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VendingMachine {
	private static final String POUND = "POUND";
	private static final String FIFTY_PENCE = "FIFTY_PENCE";
	private static final String TWENTY_PENCE = "TWENTY_PENCE";
	private static final String TEN_PENCE = "TEN_PENCE";
	private boolean onOffFlag;
	private BigDecimal balance;
	private List<Item> items;
	private Map<String , Integer> change;
	
	public VendingMachine() {
		change = new HashMap<String, Integer>();
		balance = BigDecimal.ZERO;
	}
	
	public VendingMachine(List<Item> items) {
		this();
		this.items = items;
	}
	
	public void loadChange(Map<String , Integer> loadChange) {
		for(String key : loadChange.keySet()) {
			Integer loadChangeCount = loadChange.get(key);
			Integer changeCount = change.get(key);
			this.change.put(key, loadChangeCount == null ? 0 : loadChangeCount + (changeCount == null ? 0 : changeCount));
		}
	}
	
	public boolean isOn() {
		return onOffFlag;
	}
	
	public void setOn() {
		onOffFlag = true;
	}
	
	public void setOff() {
		onOffFlag = false;
	}
	
	public double getBalance() {
		return balance.doubleValue();
	}

	public void insertMoney(Double money) {
		boolean validate = validateMoney(money);
		if (!validate) {
			throw new RuntimeException("Invalid amount " + money);
		}
		balance = balance.add(new BigDecimal(money.toString()));
		String moneyValue = null;
		if(money == 0.10) {
			moneyValue = TEN_PENCE;
		} else if (money == 0.20) {
			moneyValue = TWENTY_PENCE;
		} else if (money == 0.50) {
			moneyValue = FIFTY_PENCE;
		} else if(money == 1) {
			moneyValue = POUND;
		}
		Integer count = change.get(moneyValue);
		change.put(moneyValue, count == null ? 1 : count + 1);
	}
	
	protected boolean validateMoney(double money) {
		boolean retVal = false;
		retVal = money == 0.10 || money == 0.20 || money == 0.50 || money == 1.00; 
		return retVal;
	}
	
	public Map<String,Integer> returnCoins() {
		Map<String,Integer> retVal = getChange(balance.doubleValue());
		recalculateChange(retVal);
		balance =  BigDecimal.ZERO;
		return retVal;
	}
	
	public Map<String, Integer> getChange() {
		return change;
	}

	private void recalculateChange(Map<String, Integer> retVal) {
		for(String key : retVal.keySet()) {
			Integer count = change.get(key);
			change.put(key, count == null ? 0 : count - retVal.get(key));
		}
	}

	public Map<String, Integer> getChange(Double amount) {
		Map<String, Integer> retVal = new HashMap<String, Integer>();
		for(BigDecimal val = new BigDecimal(amount.toString()); val.doubleValue() > 0;) {
			String moneyValue = null;
			if(val.doubleValue() >=1) {
				val = val.subtract(new BigDecimal("1.00"));
				moneyValue = POUND;
			} else if(val.doubleValue() >=0.50) {
				val = val.subtract(new BigDecimal("0.50"));
				moneyValue = FIFTY_PENCE;
			} else if(val.doubleValue() >=0.20) {
				val = val.subtract(new BigDecimal("0.20"));
				moneyValue = TWENTY_PENCE;
			} else if(val.doubleValue() >=0.10) {
				val = val.subtract(new BigDecimal("0.10"));
				moneyValue = TEN_PENCE;
			}
			Integer count = retVal.get(moneyValue);
			retVal.put(moneyValue, count == null ? 1 : count + 1);
		}
		return retVal;
	}

	public Item getItem(String itemType) {
		Item item = items.get(items.indexOf(new Item(itemType)));
		
		if (0 == item.getQuantity()) {
			throw new RuntimeException("Item quantity is 0");
		}
		if (balance.doubleValue() < item.getPrice()) {
			throw new RuntimeException("Insufficent funds");
		}
		item.setQuantity(item.getQuantity() - 1);
		balance = balance.subtract(new BigDecimal(String.valueOf(item.getPrice())));
		
		return item;
	}

}
