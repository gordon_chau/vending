package machine;

public class Item {
	private String type;
	private int quantity;
	private double price;

	public Item(String type) {
		this.type = type;
	}

	public Item(String type, int quantity, double price) {
		this.type = type;
		this.quantity = quantity;
		this.price = price;
	}

	public String getSelector() {
		return type;
	}

	public int getQuantity() {
		return quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean retVal = true;
		
		if (obj == null) {
			retVal = false;
		}
		if (getClass() != obj.getClass()) {
			retVal = false;
		}
		
		Item other = (Item) obj;
		if (type == null) {
			if (other.type != null) {
				retVal = false;
			}
		} else if (!type.equals(other.type)) {
			retVal = false;
		}
		
		return retVal;
	}

}
